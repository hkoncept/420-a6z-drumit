package ca.cs.multimedia.drumit.models;

public class Bounds {
    private Point mUpperLeft;
    private Point mLowerRight;

    public Bounds(Point upperLeft, Point lowerRight) {
        mUpperLeft = upperLeft;
        mLowerRight = lowerRight;
    }

    public Point getUpperLeft() {
        return mUpperLeft;
    }

    public Point getLowerRight() {
        return mLowerRight;
    }
}
