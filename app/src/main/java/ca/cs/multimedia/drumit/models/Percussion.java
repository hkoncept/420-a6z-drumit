package ca.cs.multimedia.drumit.models;

public class Percussion {

    private Bounds mBounds;
    private int mSoundID;
    private String mName;


    public Percussion(String name) {
        this.mName = name;
    }

    public Percussion(String name, Bounds bounds) {
        this.mName = name;
        this.mBounds = bounds;
    }

    public void setSoundID(int soundID) {
        this.mSoundID = soundID;
    }

    public void setBounds(Bounds mBounds) {
        this.mBounds = mBounds;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public Bounds getBounds() {
        return mBounds;
    }

    public int getSoundID() {
        return mSoundID;
    }

    public String getName() {
        return mName;
    }
}
